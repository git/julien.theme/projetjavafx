<h1 align="center">Welcome to 2D Dice Game 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/Java-11-blue.svg"  alt="."/>
  <img src="https://img.shields.io/badge/JavaFX-11-yellow.svg"  alt="."/>
  <a href="https://github.com/ZartaX0O3/discord-bde-v4#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>

</p>

> This is a 2D Game in Java & JavaFX

### 🏠 [Homepage](https://github.com/ZartaX0O3/discord-bde-v4)

## Prerequisites

- Java 11 & JavaFX 11



## Usage

java --module-path ""C:/Program Files/Java/javafx-sdk-11/lib" " --add-modules javafx.fxml,javafx.controls,javafx.media -jar out/artifacts/2DGameJavaFX_jar/2DGameJavaFX.jar
```


## Author

👤 **ZartaX0O3**

* Github: [@Arthur_VD](https://gitlab.iut-clermont.uca.fr/arvandamme)
* Github: [@Julien_T](https://gitlab.iut-clermont.uca.fr/jutheme)

## Show your support

Give a ⭐️ if this project helped you!

