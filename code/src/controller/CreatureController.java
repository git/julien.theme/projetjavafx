package controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import model.Case;
import model.Creature;
import model.Map;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CreatureController {
    private int lv;
    //private final Image dog1 = new Image("/image/creature/dog1.2.png");
    private Creature creature;
    @FXML public ImageView creatureImageView;
    @FXML public AnchorPane levelRoot;
    private final Map map;
    private MapCreatureController mapCreatureController;
    private Case caseCreatureStart;
    private Thread Creature;
    private String imagePath;
    private Case caseActuelle;
    private Case caseAncienne;
    private Image imagePerso1;
    private Image imagePerso2;
    private Image imagePerso3;
    private Image imagePerso4;
    private Image imagePerso5;
    private Image imagePerso6;
    private Image imagePerso7;
    private Image imagePerso8;
    private Image imagePerso9;
    private Image imagePerso10;
    private Image imagePerso11;
    private Image imagePerso12;

    /**
     * Constructor of the CreatureController class
     * @param lv
     * @param map
     * @param creatureImageView
     */
    public CreatureController(int lv,Map map, ImageView creatureImageView) {
        this.lv = lv;
        this.map = map;
        this.creatureImageView = creatureImageView;
    }

    /**
     * Method to create a creature
     * @param animal
     */
    public void createCreature(int animal) {
        if(animal == 1) {
            this.creature = new Creature(1,"/image/creature/dog1.",0,0  );
            //creatureImageView.setImage(dog1);
        }
        if(animal == 2) {
            this.creature = new Creature(2,"/image/creature/bear1.",0,0  );
        }
        if(animal == 3) {
            this.creature = new Creature(2,"/image/creature/goat1.",0,0  );
        }
    }

    /**
     * Method to initialize the creature
     * @throws InterruptedException
     */
    public void playerInitialisation() throws InterruptedException {
        mapCreatureController = new MapCreatureController(lv, map);
        mapCreatureController.creerMapCreature();
        int max = 3;
        int min = 1;
        double a = Math.random() * ( max - min );
        int numCreature = (int) Math.round(a) + min;
        createCreature(numCreature);

        caseCreatureStart = mapCreatureController.getStartCase();
        creature.setCoordX(caseCreatureStart.getCoordX());
        creature.setCoordY(caseCreatureStart.getCoordY());
        caseActuelle = caseCreatureStart;

        imagePath = getPath();
        String pathStart = imagePath + "2.png";
        creatureImageView.setImage(new Image(pathStart));
        creatureImageView.setId("creatureImageView");
        creatureImageView.setFitWidth(35);
        creatureImageView.setFitHeight(35);
        creatureImageView.setLayoutX(getX()*map.getSizeTile()-5);
        creatureImageView.setLayoutY(getY()*map.getSizeTile()-5);
        //levelRoot.getChildren().addAll(creatureImageView);

        Creature = new Thread("Creature") {
            public void run() {
                for (int i = 0; i < mapCreatureController.getSizeListeCase()-1; i++) {
                    try {
                        movePlayerNextCase();
                        Creature.sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if(i>=mapCreatureController.getSizeListeCase()-2) {
                        creatureImageView.setImage(new Image("/image/creature/void.png"));
                        System.out.println("FIN 1");
                    }
                }
                System.out.println("FIN 2");
                creatureImageView.setImage(new Image("/image/creature/void.png"));
            }
        };
        Creature.start();


    }

    /**
     * Method to move the creature to the next case
     * @throws InterruptedException
     */
    public void movePlayerNextCase() throws InterruptedException {
        caseAncienne = caseActuelle;
        caseActuelle = mapCreatureController.getNextCase(caseActuelle);
        Timeline timelines = new Timeline();
        timelines.setCycleCount(1);
        int time1 = 100;
        int time2 = 200;
        int time3 = 300;
        int time4 = 400;
        int time5 = 500;
        int time6 = 600;
        int time7 = 700;
        int time8 = 800;
        int avancementCase = 4;
        int tempsattente = 00;
        //animation déplacement vers la droite
        if(caseActuelle.getCoordX() > caseAncienne.getCoordX()){
            imagePerso7 = new Image(imagePath + "7.png");
            imagePerso8 = new Image(imagePath + "8.png");
            imagePerso9 = new Image(imagePath + "9.png");
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                creatureImageView.setImage(imagePerso8);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                creatureImageView.setImage(imagePerso7);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                creatureImageView.setImage(imagePerso9);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                creatureImageView.setImage(imagePerso7);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                creatureImageView.setImage(imagePerso9);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                creatureImageView.setImage(imagePerso7);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                creatureImageView.setImage(imagePerso9);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                creatureImageView.setImage(imagePerso8);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()+avancementCase);
            }));
            timelines.play();
        }
        //animation déplacement vers la gauche
        if(caseActuelle.getCoordX() < caseAncienne.getCoordX()){
            imagePerso4 = new Image(imagePath + "4.png");
            imagePerso5 = new Image(imagePath + "5.png");
            imagePerso6 = new Image(imagePath + "6.png");
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                creatureImageView.setImage(imagePerso5);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                creatureImageView.setImage(imagePerso4);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                creatureImageView.setImage(imagePerso6);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                creatureImageView.setImage(imagePerso4);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                creatureImageView.setImage(imagePerso6);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                creatureImageView.setImage(imagePerso4);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                creatureImageView.setImage(imagePerso6);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                creatureImageView.setImage(imagePerso5);
                creatureImageView.setLayoutX(creatureImageView.getLayoutX()-avancementCase);
            }));
            timelines.play();
        }
        //animation déplacement vers le haut
        if(caseActuelle.getCoordY() < caseAncienne.getCoordY()){
            imagePerso10 = new Image(imagePath + "10.png");
            imagePerso11 = new Image(imagePath + "11.png");
            imagePerso12 = new Image(imagePath + "12.png");
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                creatureImageView.setImage(imagePerso11);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                creatureImageView.setImage(imagePerso10);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                creatureImageView.setImage(imagePerso12);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                creatureImageView.setImage(imagePerso10);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                creatureImageView.setImage(imagePerso12);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                creatureImageView.setImage(imagePerso10);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                creatureImageView.setImage(imagePerso12);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                creatureImageView.setImage(imagePerso11);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()-avancementCase);
            }));
            timelines.play();
        }
        //animation déplacement vers le bas
        if(caseActuelle.getCoordY() > caseAncienne.getCoordY()){
            imagePerso1 = new Image(imagePath + "1.png");
            imagePerso2 = new Image(imagePath + "2.png");
            imagePerso3 = new Image(imagePath + "3.png");
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                creatureImageView.setImage(imagePerso2);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                creatureImageView.setImage(imagePerso1);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                creatureImageView.setImage(imagePerso3);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                creatureImageView.setImage(imagePerso1);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                creatureImageView.setImage(imagePerso3);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                creatureImageView.setImage(imagePerso1);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                creatureImageView.setImage(imagePerso3);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                creatureImageView.setImage(imagePerso2);
                creatureImageView.setLayoutY(creatureImageView.getLayoutY()+avancementCase);
            }));
            timelines.play();
        }
    }

    /**
     * Method to get the path of the image of the creature
     * @return the path of the image of the creature
     */
    public String getPath(){
        return creature.getImage();
    }

    /**
     * Method to get the X coordinate of the creature
     * @return X coordinate
     */
    public int getX(){
        return creature.getCoordX();

    }

    /**
     * Method to get the Y coordinate of the creature
     * @return Y coordinate
     */
    public int getY(){
        return creature.getCoordY();
    }
}
