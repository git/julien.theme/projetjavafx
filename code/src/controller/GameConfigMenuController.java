package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class GameConfigMenuController implements Initializable {

    @FXML private AnchorPane levelRoot;

    @FXML private RadioButton radioButton1;
    @FXML private RadioButton radioButton2;
    @FXML private RadioButton radioButton3;
    @FXML private Button button1;
    @FXML private MenuItem choice1;
    @FXML private MenuItem choice2;
    @FXML private MenuItem choice3;
    @FXML private ImageView ImageChange;
    @FXML private ToggleGroup group;

    private int personnage;
    private int difficulty;

    /**
     * Initializes the controller class.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        radioButton1.setToggleGroup(group);
        radioButton2.setToggleGroup(group);
        radioButton3.setToggleGroup(group);
    }

    /**
     * Method to change the skin of the player one
     * @param event
     */
    @FXML void changeSkin1(ActionEvent event){
            ImageChange.setImage(new Image("/image/personnage/Perso1.2.png"));
            this.personnage = 1;
    }

    /**
     * Method to change the skin of the player two
     * @param event
     */
    @FXML void changeSkin2(ActionEvent event){
        ImageChange.setImage(new Image("/image/personnage/Perso2.2.png"));
        this.personnage = 2;
    }

    /**
     * Method to change the skin of the player three
     * @param event
     */
    @FXML void changeSkin3(ActionEvent event){
        ImageChange.setImage(new Image("/image/personnage/Perso3.2.png"));
        this.personnage = 3;
    }

    /**
     * Method to set the difficulty to 1
     * @param event
     */
    @FXML void setDifficulty1(MouseEvent event){
        this.difficulty = 1;
    }

    /**
     * Method to set the difficulty to 2
     * @param event
     */
    @FXML void setDifficulty2(MouseEvent event){
        this.difficulty = 2;
    }

    /**
     * Method to set the difficulty to 3
     * @param event
     */
    @FXML void setDifficulty3(MouseEvent event){
        this.difficulty = 3;
    }

    /**
     * Method to start the game
     * @param event
     * @throws Exception
     */
    @FXML
    void launchGame(MouseEvent event) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/viewGame2.fxml"));
        Parent pane = loader.load();
        GamePlayController gp =  loader.getController();
        if(this.difficulty == 0){
            this.difficulty = 1;
        }
        gp.difficulty = this.difficulty;
        if(this.personnage == 0){
            this.personnage = 1;
        }
        gp.perso = this.personnage;
        gp.gameLauncher();

        Stage a = new Stage();
        a.setScene(new Scene(pane, 1300, 900));
        a.setResizable(false);
        a.show();
        Stage stage = (Stage) levelRoot.getScene().getWindow();
        stage.close();
    }

    /**
     * Method to go back to the main menu
     * @param event
     * @throws Exception
     */
    @FXML
    void returnToMenu(MouseEvent event) throws Exception{
        AnchorPane pane = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/MainPage.fxml")));
        levelRoot.getChildren().setAll(pane);
    }

}
