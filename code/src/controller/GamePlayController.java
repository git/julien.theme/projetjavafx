package controller;

import com.jfoenix.controls.JFXButton;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import model.*;
import model.Case;
import model.GameLoop;
import model.GameTime;
import model.Map;
import model.ResultatTxt;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class GamePlayController implements Initializable {

    private final int avancementCase = 4;
    private final PersoController persoController = new PersoController();
    private final MapController mapController = new MapController();

        @FXML public ImageView persoImageView;
        @FXML public AnchorPane levelRoot;
        @FXML public ImageView de;
        @FXML public ImageView creatureImageView = new ImageView();
        @FXML public ImageView playerImagePerso;


    private boolean finJeu = false;
    private GameTime gameTime;
    private GameLoop loop;
    private Text textEnd;
    private Text resultat1;
    private Text resultat2;
    private Text resultat3;
    private Text resultat4;
    private Text resultat5;
    private int nbTour = 0;
    private int numCase = 0;
    int l;

    public int difficulty;
    public int perso;
    private Case caseActuelle, caseAncienne;
    private int i, diceNumber;
    private Thread personnage;
    private String pathPerso;
    private JFXButton button;
    private Boolean PersoMouv;

    /**
     * Initializes the controller class.
     * @param url
     * @param resourceBundle
     */
    @FXML
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    }

    /**
     * Method to launch the game
     */
    public void gameLauncher(){
            levelRoot.getChildren().addAll(mapController.mapInitialisation(this.difficulty), persoController.playerInitialisation(this.difficulty,this.perso, mapController.getMap()), creatureImageView);
            persoController.setActualCase(mapController.getStartCase());
            playerImagePerso.setImage(new Image("image/personnage/Perso" + this.perso + ".2.png"));
            loop = new GameLoop(this.difficulty, mapController.getMap(), creatureImageView);
            loop.start();
            Text text = new Text();
            text.setText("Time : ");
            text.setX(1000);
            text.setY(50);
            text.setFill(Color.WHITE);
            text.setStyle("-fx-font: 20 arial;");
            Text textTimer = new Text();
            textTimer.setText("");
            textTimer.setX(1070);
            textTimer.setY(50);
            textTimer.setFill(Color.WHITE);
            textTimer.setStyle("-fx-font: 20 arial;");
            textEnd = new Text();
            textEnd.setText("");
            textEnd.setX(500);
            textEnd.setY(500);
            textEnd.setFill(Color.WHITE);
            textEnd.setStyle("-fx-font: 100 arial; ");
            resultat1 = new Text();
            resultat1.setText("");
            resultat1.setX(1045);
            resultat1.setY(350);
            resultat1.setFill(Color.WHITE);
            resultat1.setStyle("-fx-font: 20 arial; ");
            resultat2 = new Text();
            resultat2.setText("");
            resultat2.setX(1045);
            resultat2.setY(400);
            resultat2.setFill(Color.WHITE);
            resultat2.setStyle("-fx-font: 20 arial; ");
            resultat3 = new Text();
            resultat3.setText("");
            resultat3.setX(1045);
            resultat3.setY(450);
            resultat3.setFill(Color.WHITE);
            resultat3.setStyle("-fx-font: 20 arial; ");
            resultat4 = new Text();
            resultat4.setText("");
            resultat4.setX(1045);
            resultat4.setY(500);
            resultat4.setFill(Color.WHITE);
            resultat4.setStyle("-fx-font: 20 arial; ");
            resultat5 = new Text();
            resultat5.setText("");
            resultat5.setX(1045);
            resultat5.setY(550);
            resultat5.setFill(Color.WHITE);
            resultat5.setStyle("-fx-font: 20 arial; ");
            button = new JFXButton("");
            levelRoot.getChildren().addAll(text,textTimer,textEnd,button, resultat1, resultat2, resultat3, resultat4, resultat5);
            gameTime = new GameTime(textTimer);
            gameTime.start();
            PersoMouv = false;
        }

    /**
     * Method to stop the gameTime and the gameLoop
     */
    public void timer(){
            if(finJeu){
                gameTime.stop();
                loop.stop();
            }
        }

    /**
     * Method to move the player to the next case
     * @throws InterruptedException
     */
    public void movePlayerNextCase() throws InterruptedException {

        persoImageView = persoController.persoImageView;
        caseActuelle = persoController.getActualCase();
        pathPerso = persoController.getPath();
        caseAncienne = caseActuelle;
        persoController.setOldCase(caseAncienne);
        caseActuelle = mapController.getNextCase(caseActuelle);
        persoController.setActualCase(caseActuelle);
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);

            int time1 = 100;
            int time2 = 200;
            int time3 = 300;
            int time4 = 400;
            int time5 = 500;
            int time6 = 600;
            int time7 = 700;
            int time8 = 800;
            if(caseActuelle != null){
                //animation déplacement vers la droite
                if (caseActuelle.getCoordX() > caseAncienne.getCoordX()) {
                    Image imagePerso7 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "7.png")).toExternalForm());
                    Image imagePerso8 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "8.png")).toExternalForm());
                    Image imagePerso9 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "9.png")).toExternalForm());
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                        persoImageView.setImage(imagePerso8);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                        persoImageView.setImage(imagePerso7);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                        persoImageView.setImage(imagePerso9);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                        persoImageView.setImage(imagePerso7);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                        persoImageView.setImage(imagePerso9);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                        persoImageView.setImage(imagePerso7);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                        persoImageView.setImage(imagePerso9);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                        persoImageView.setImage(imagePerso8);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() + avancementCase);
                    }));
                    timeline.play();
                }
                //animation déplacement vers la gauche
                if (caseActuelle.getCoordX() < caseAncienne.getCoordX()) {
                    Image imagePerso4 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "4.png")).toExternalForm());
                    Image imagePerso5 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "5.png")).toExternalForm());
                    Image imagePerso6 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "6.png")).toExternalForm());
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                        persoImageView.setImage(imagePerso5);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                        persoImageView.setImage(imagePerso4);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                        persoImageView.setImage(imagePerso6);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                        persoImageView.setImage(imagePerso4);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                        persoImageView.setImage(imagePerso6);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                        persoImageView.setImage(imagePerso4);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                        persoImageView.setImage(imagePerso6);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                        persoImageView.setImage(imagePerso5);
                        persoImageView.setLayoutX(persoImageView.getLayoutX() - avancementCase);
                    }));
                    timeline.play();
                }
                //animation déplacement vers le haut
                if (caseActuelle.getCoordY() < caseAncienne.getCoordY()) {
                    Image imagePerso10 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "10.png")).toExternalForm());
                    Image imagePerso11 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "11.png")).toExternalForm());
                    Image imagePerso12 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "12.png")).toExternalForm());
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                        persoImageView.setImage(imagePerso11);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                        persoImageView.setImage(imagePerso10);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                        persoImageView.setImage(imagePerso12);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                        persoImageView.setImage(imagePerso10);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                        persoImageView.setImage(imagePerso12);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                        persoImageView.setImage(imagePerso10);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time7), event -> {
                        persoImageView.setImage(imagePerso12);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time8), event -> {
                        persoImageView.setImage(imagePerso11);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() - avancementCase);
                    }));
                    timeline.play();
                }
                //animation déplacement vers le bas
                if (caseActuelle.getCoordY() > caseAncienne.getCoordY()) {
                    Image imagePerso1 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "1.png")).toExternalForm());
                    Image imagePerso2 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "2.png")).toExternalForm());
                    Image imagePerso3 = new Image(Objects.requireNonNull(getClass().getClassLoader().getResource(pathPerso + "3.png")).toExternalForm());
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time1), event -> {
                        persoImageView.setImage(imagePerso2);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time2), event -> {
                        persoImageView.setImage(imagePerso1);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time3), event -> {
                        persoImageView.setImage(imagePerso3);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time4), event -> {
                        persoImageView.setImage(imagePerso1);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time5), event -> {
                        persoImageView.setImage(imagePerso3);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(time6), event -> {
                        persoImageView.setImage(imagePerso1);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(800), event -> {
                        persoImageView.setImage(imagePerso3);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(900), event -> {
                        persoImageView.setImage(imagePerso2);
                        persoImageView.setLayoutY(persoImageView.getLayoutY() + avancementCase);
                    }));
                    timeline.play();
                }
                if(caseActuelle.isEnd()){
                    textEnd.setText("Fin");
                    finJeu = true;
                    timer();
                    returnToStart();
                    saveScore();
                }
            }else{
                System.out.println("Case null");

            }
        }

    /**
     * Method to return to the start of the game
     */
    public void returnToStart(){
        button.setText("Exit");
        button.setStyle("-fx-background-color: #EB9C0A; -fx-text-fill: white;");
        button.setLayoutX(1071);
        button.setLayoutY(752);
        button.setPrefHeight(30);
        button.setPrefWidth(119);
        button.setOnAction(event -> {
            System.exit(0);
        }
        );
    }

    /**
     * Method to lunch the dice
     */
    public void lancerDe() {
        Case actuel = persoController.getActualCase();;

        if(!PersoMouv) {
            animationDe();
            nbTour++;
        }
    }


    /**
     * Method to launch the animation of the dice
     */
    public void animationDe() {
        personnage = new Thread("personnage") {

            public void run() {
                i = 0;
                Timeline timeline = new Timeline();
                timeline.setCycleCount(1);

                for (i = 0; i < 10; i++) {
                    diceNumber = (int) (Math.random() * 6 + 1);
                    String path = "image/dé/d" + diceNumber + ".png";
                    timeline.getKeyFrames().add(new KeyFrame(Duration.millis(50 * i), event -> de.setImage(new Image(path))));
                }
                Map map = mapController.getMap();
                Case c;
                if(map.getMap().size() > numCase+diceNumber){
                    c = map.getMap().get(numCase+diceNumber);
                }
                else {
                    c = map.getMap().get(map.getMap().size()-1);
                }

                if(c.isBonus()){
                    Bonus bonus = Bonus.randomGet();
                    switch (Objects.requireNonNull(bonus)){
                        case MOVE_FORWARD_1:
                            System.out.println("AJOUT DE 1");
                            diceNumber+=1;
                            break;

                        case MOVE_FORWARD_3:
                            System.out.println("AJOUT DE 3");
                            diceNumber+=3;
                            break;

                        case MOVE_FORWARD_5:
                            System.out.println("AJOUT DE 5");
                            diceNumber+=5;
                            break;

                        case MOVE_FORWARD_8:
                            System.out.println("AJOUT DE 8");
                            diceNumber+=8;
                            break;
                    }
                }
                else if(c.isMalus()){
                    Malus malus = Malus.randomGet();
                    switch (Objects.requireNonNull(malus)){
                        case MOVE_BACKWARD_1:
                            System.out.println("SUPRESSION DE 1");
                            diceNumber-=1;
                            break;

                        case MOVE_BACKWARD_3:
                            System.out.println("SUPRESSION DE 3");
                            if(diceNumber > 3){
                                diceNumber-=3;
                            } else  {
                                diceNumber=0;
                            }
                            break;

                        case MOVE_BACKWARD_5:
                            System.out.println("SUPRESSION DE 5");
                            if(diceNumber > 5){
                                diceNumber-=5;
                            } else  {
                                diceNumber=0;
                            }
                            break;

                        case LAST_POSITION:
                            System.out.println("SUPRESSION DE DICE");
                            diceNumber=0;
                            break;
                    }
                }
                numCase+=diceNumber;

                timeline.play();
                try {
                    personnage.sleep(700);
                    affichageResultat();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                for (i = 0; i < diceNumber; i++) {
                    try {
                        PersoMouv = true;
                        movePlayerNextCase();
                        sleep(800);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println(persoController.getActualCase().getCoordX() + " " + persoController.getActualCase().getCoordY());
                System.out.println(numCase);
                PersoMouv = false;
            }
        };
        personnage.start();
    }

    /**
     * Method to display the result of the dice
     */
    public void affichageResultat(){
        String ancienResultat1;
        String ancienResultat2;
        String ancienResultat3;
        String ancienResultat4;
        String ancienResultat5;
        if(nbTour == 1){
            resultat1.setText("Vous avez fait un " + diceNumber + ".");
        }else{
            if(nbTour == 2){
                ancienResultat1 = resultat1.getText();
                resultat1.setText("Vous avez fait un " + diceNumber + ".");
                resultat2.setText(ancienResultat1);
                System.out.println("ancienResultat2 : " + ancienResultat1);
            }else{
                if(nbTour == 3) {
                    ancienResultat1 = resultat1.getText();
                    ancienResultat2 = resultat2.getText();
                    resultat1.setText("Vous avez fait un " + diceNumber + ".");
                    resultat2.setText(ancienResultat1);
                    resultat3.setText(ancienResultat2);
                }else{
                    if(nbTour == 4){
                        ancienResultat1 = resultat1.getText();
                        ancienResultat2 = resultat2.getText();
                        ancienResultat3 = resultat3.getText();
                        resultat1.setText("Vous avez fait un " + diceNumber + ".");
                        resultat2.setText(ancienResultat1);
                        resultat3.setText(ancienResultat2);
                        resultat4.setText(ancienResultat3);
                    }else{
                        ancienResultat1 = resultat1.getText();
                        ancienResultat2 = resultat2.getText();
                        ancienResultat3 = resultat3.getText();
                        ancienResultat4 = resultat4.getText();
                        resultat1.setText("Vous avez fait un " + diceNumber + ".");
                        resultat2.setText(ancienResultat1);
                        resultat3.setText(ancienResultat2);
                        resultat4.setText(ancienResultat3);
                        resultat5.setText(ancienResultat4);
                    }
                }

            }
        }
    }

    /**
     * Method to save the score of the player
     */
    public void saveScore(){
        //difficulty
        int resultat = gameTime.getTime();
        ResultatTxt resultatTxt = new ResultatTxt();
        resultatTxt.sauvegarderResultat(resultat, difficulty);
    }
}
