package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import launcher.Main;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;

public class MainPageController implements Initializable {

    @FXML
    private AnchorPane mainRoot;

    @FXML
    private Button startGame;

    @FXML
    private Button viewScore;

    @FXML
    private ImageView exitGame;

    @FXML
    private ImageView loadGame;

    /**
     * Initializes the controller class.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    /**
     * Go to the score page
     * @param event
     * @throws Exception
     */
    @FXML
    void viewScore(MouseEvent event) throws  Exception{
        AnchorPane pane = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/TopPlayers.fxml")));
        mainRoot.getChildren().setAll(pane);
    }

    /**
     * Go to the game configuration page
     * @param event
     * @throws Exception
     */
    @FXML
    void startGame(MouseEvent event) throws Exception{
        AnchorPane pane = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/GameConfigurationMenu.fxml")));
        mainRoot.getChildren().setAll(pane);
    }

    /**
     * Exit the game
     * @param event
     */
    @FXML
    void exitGame(MouseEvent event) {
        System.exit(0);
    }
}
