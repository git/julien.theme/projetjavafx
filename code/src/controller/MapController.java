package controller;

import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Case;
import model.Map;

import java.util.ArrayList;

public class MapController implements Initializable {

    private final Map map = new Map(32,30);
    private final Image b = new Image("/image/terrain/chemin.png");
    private final Image a = new Image("/image/terrain/herbe.png");
    private final Image c = new Image("/image/terrain/arbre2hg.png");
    private final Image d = new Image("/image/terrain/arbre2hd.png");
    private final Image e = new Image("/image/terrain/arbre2bg.png");
    private final Image f = new Image("/image/terrain/arbre2bd.png");
    private final Image g = new Image("/image/terrain/eventail.png");
    private final Image h = new Image("/image/terrain/salade.png");
    private final Image i = new Image("/image/terrain/foin1hg.png");
    private final Image j = new Image("/image/terrain/foin1hd.png");
    private final Image k = new Image("/image/terrain/foin1bg.png");
    private final Image l = new Image("/image/terrain/foin1bd.png");
    private final Image m = new Image("/image/terrain/arbuste1.png");
    private final Image n = new Image("/image/terrain/arbuste2.png");
    private final Image o = new Image("/image/terrain/arbuste2.png");
    private final Image p = new Image("/image/terrain/arbre1h.png");
    private final Image q = new Image("/image/terrain/arbre1b.png");
    private final Image r = new Image("/image/terrain/tronc2g.png");
    private final Image s = new Image("/image/terrain/tronc2d.png");
    private final Image t = new Image("/image/terrain/tronc1g.png");
    private final Image u = new Image("/image/terrain/tronc1d.png");
    private final Image v = new Image("/image/terrain/souche1.png");
    private final Image w = new Image("/image/terrain/chemin.png");
    private final Image x = new Image("/image/terrain/multiarbre1.png");
    private final Image y = new Image("/image/terrain/multiarbre2.png");
    private final Image z1 = new Image("/image/terrain/bonus.png");
    private final Image z2 = new Image("/image/terrain/malus" + ".png");
    private final Image aa = new Image("/image/terrain/pierre2.png");
    private final Image ab = new Image("/image/terrain/pierre1.png");
    private final Image ac = new Image("/image/terrain/tombe1.png");
    private final Image ad = new Image("/image/terrain/statue1h.png");
    private final Image ae = new Image("/image/terrain/statue1b.png");
    private final Image af = new Image("/image/terrain/tronc3h.png");
    private final Image ag = new Image("/image/terrain/tronc3b.png");
    private final Image ah = new Image("/image/terrain/souche2.png");
    private final Image ai = new Image("/image/terrain/herbe2.png");
    private final Image aj = new Image("/image/terrain/puit1hg.png");
    private final Image ak = new Image("/image/terrain/puit1hd.png");
    private final Image al = new Image("/image/terrain/puit1bg.png");
    private final Image am = new Image("/image/terrain/puit1bd.png");
    private final int[][] coordFirstDifficultyMap = {{0,24},{1,24},{2,24},{3,24},{4,24},{5,24},{5,23},{5,22},{5,21},{5,20},{5,19},{5,18},{6,18},{7,18},{8,18},{9,18},{10,18},{11,18},{12,18},{12,17},{12,16},{12,15},{12,14},{12,13},{12,12},{11,12},{10,12},{9,12},{8,12},{7,12},{6,12},{5,12},{4,12},{3,12},{3,11},{3,10},{3,9},{3,8},{3,7},{3,6},{3,5},{3,4},{3,3},{4,3},{5,3},{6,3},{7,3},{8,3},{9,3},{10,3},{11,3},{12,3},{13,3},{14,3},{15,3},{16,3},{17,3},{18,3},{19,3},{19,4},{19,5},{19,6},{19,7},{19,8},{19,9},{19,10},{20,10},{21,10},{22,10},{23,10},{24,10},{24,11},{24,12},{24,13},{24,14},{24,15},{24,16},{24,17},{24,18},{24,19},{24,20},{24,21},{24,22},{24,23},{24,24},{25,24},{26,24},{27,24},{28,24},{29,24}};
    private final int[][] coordSecondDifficultyMap = {{17,0},{17,1},{17,2},{17,3},{17,4},{18,4},{19,4},{20,4},{21,4},{22,4},{23,4},{24,4},{25,4},{25,5},{25,6},{25,7},{25,8},{25,9},{25,10},{25,11},{25,12},{25,13},{25,14},{24,14},{23,14},{22,14},{21,14},{20,14},{20,15},{19,15},{18,15},{17,15},{16,15},{15,15},{15,14},{15,13},{15,12},{15,11},{15,10},{14,10},{13,10},{12,10},{11,10},{10,10},{9,10},{8,10},{7,10},{6,10},{5,10},{4,10},{3,10},{2,10},{2,11},{2,12},{2,13},{2,14},{2,15},{2,16},{2,17},{2,17},{3,17},{4,17},{5,17},{6,17},{7,17},{8,17},{9,17},{9,18},{9,19},{9,20},{9,21},{9,22},{9,23},{9,24},{9,25},{10,25},{11,25},{12,25},{13,25},{14,25},{15,25},{16,25},{17,25},{18,25},{19,25},{20,25},{21,25},{22,25},{23,25},{24,25},{25,25},{26,25},{27,25},{28,25},{29,25}};
    private final int[][] coordThirdDifficultyMap = {{28,0},{28,1},{28,2},{28,3},{27,3},{26,3},{25,3},{24,3},{23,3},{22,3},{21,3},{20,3},{19,3},{18,3},{17,3},{16,3},{15,3},{14,3},{13,3},{12,3},{11,3},{10,3},{9,3},{8,3},{7,3},{6,3},{5,3},{4,3},{3,3},{3,4},{3,5},{3,6},{3,7},{4,7},{5,7},{6,7},{7,7},{8,7},{9,7},{10,7},{10,8},{10,9},{11,9},{12,9},{13,9},{14,9},{15,9},{16,9},{17,9},{18,9},{19,9},{20,9},{21,9},{22,9},{23,9},{23,10},{23,11},{23,12},{23,13},{23,14},{24,14},{25,14},{26,14},{26,15},{26,16},{26,17},{26,18},{26,19},{26,20},{26,21},{26,22},{26,23},{26,24},{26,25},{25,25},{24,25},{23,25},{22,25},{21,25},{20,25},{19,25},{18,25},{17,25},{16,25},{15,25},{14,25},{13,25},{12,25},{11,25},{10,25},{9,25},{8,25},{7,25},{6,25},{5,25},{4,25},{3,25},{2,25},{1,25},{1,24},{1,23},{1,22},{1,21},{1,20},{1,19},{2,19},{3,19},{4,19},{5,19},{6,19},{7,19},{8,19},{9,19},{10,19},{11,19},{12,19},{13,19},{13,18},{13,17},{13,16},{13,15},{13,14},{12,14},{11,14},{10,14},{9,14},{8,14},{7,14},{6,14},{5,14},{4,14},{3,14},{2,14},{1,14},{0,14}};
    private int[][] coordMap = {};

    /**
     * Initializes the controller class.
     * @param location
     * @param resources
     */
    @Override
    public void initialize(java.net.URL location, java.util.ResourceBundle resources) {
    }

    /**
     * Method which create the map
     * @param difficulty
     */
    public void createMap(int difficulty) {

         int j = 1, k;
         if(difficulty==1){
             coordMap = coordFirstDifficultyMap;
         }else{
             if (difficulty==2){
                coordMap = coordSecondDifficultyMap;
             }else{
                 coordMap = coordThirdDifficultyMap;
             }
         }
            for(int[] i : coordMap){
                boolean boolStart = j == 1;
                boolean boolEnd = j == coordMap.length;
                boolean bonus = false, malus = false;

                if(difficulty == 1) k = (int) (Math.random() * 10);
                else if(difficulty == 2) k = (int) (Math.random() * 8);
                else k = (int) (Math.random() * 6);

                if(k == 1 || k == 2) bonus = true;
                else if(k == 3) malus = true;

                Case case1 = new Case(j, i[0], i [1], boolStart, bonus, malus, boolEnd, map);
                map.addCase(case1);
                j++;
            }


        if (difficulty != 1 && difficulty != 2 && difficulty != 3) {
            System.out.println("Difficulty not found");
        }
    }

    /**
     * Method which initialize the map
     * @param difficulty
     * @return
     */
    public AnchorPane mapInitialisation(int difficulty) {

        int tileSize = 32;
        if(difficulty == 1){

            Image[][] grid = {
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,b,b,b,b,b,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a},
                    {b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,b,b,b,b,b},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a}
            };

            Image[][] grid2 = {
                    {o,o,o,o,m,o,o,o,o,o,c,d,o,o,o,o,o,o,o,o,o,o,o,c,x,y,x,y,x,y},
                    {o,o,o,o,o,o,o,o,o,o,e,f,o,o,o,o,o,o,n,o,o,o,o,e,y,x,y,x,y,x},
                    {c,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,e,y,x,y},
                    {e,f,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,m,o,o,o,e,f,e},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,t,u,o},
                    {n,o,o,o,o,o,o,o,n,o,o,o,o,c,d,o,o,o,o,o,o,o,n,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,o,m,o,o,o,o,o,o,o,o,c,d,o,o,o},
                    {o,o,o,o,o,o,c,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,o,o,o},
                    {o,m,o,o,o,o,e,f,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,p,o,o,o,o,o,o,o,o,o,o,o,m,o,o,o,o,p,o,o,o,o,o,o,o,o,p,o,o},
                    {o,q,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,o,o,o,o,o,o,q,o,o},
                    {o,o,g,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {h,h,h,o,o,i,j,o,i,j,o,o,o,o,n,o,o,o,n,o,o,o,o,o,o,o,o,o,o,o},
                    {h,h,h,o,o,k,l,o,k,l,o,o,o,o,o,o,o,o,o,c,d,o,o,o,o,o,o,o,c,d},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,p,o,o,e,f,o,o,o,o,o,o,o,e,f},
                    {c,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {e,f,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,n,o,o,o,o,o,o,o,o,m,o,o,o,o,p,o,o},
                    {o,o,o,m,o,o,o,o,o,p,o,o,o,o,o,o,o,r,s,o,o,o,o,o,o,o,o,q,o,o},
                    {n,o,o,o,o,o,o,o,o,q,o,o,o,c,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,o,p,o,v,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,n,o,o,p,o,o,o,o,o,o,o,o,n,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,o,o,o,c,d,o,o,o,o,o,o,m,o,o,o},
                    {c,d,c,d,c,d,c,d,o,o,o,o,o,o,o,n,o,o,e,f,o,o,p,o,o,o,o,o,o,o},
                    {x,y,x,y,x,y,x,y,d,o,r,s,o,o,o,o,o,o,o,o,o,o,q,o,o,c,d,o,o,o},
                    {y,x,y,x,y,x,y,x,y,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,o,o,o}
            };

            GridPane gridPane = new GridPane();

            // for visualizing the different squares:

            gridPane.setStyle("-fx-background-color: grey;");

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    ImageView imageView = new ImageView(grid[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            for (int y = 0 ; y < grid2.length ; y++) {
                for (int x = 0 ; x < grid2[y].length ; x++) {
                    if(grid2[y][x] == o) continue;
                    ImageView imageView = new ImageView(grid2[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            createMap(difficulty);
            ArrayList<Case> array = map.getMap();

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    if(grid[y][x] != b) continue;
                    for(Case c : array) {
                        if (c.getCoordY() == y && c.getCoordX() == x && c.isBonus()) {
                            ImageView imageView = new ImageView(z1);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                        else if(c.getCoordY() == y && c.getCoordX() == x && c.isMalus()){
                            ImageView imageView = new ImageView(z2);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                    }
                }
            }

            AnchorPane FirstAnchor = new AnchorPane();
            FirstAnchor.getChildren().addAll(gridPane);
            return FirstAnchor;
        }

        else if(difficulty == 2){

            Image[][] grid = {
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,b,b,b,b,b,b,b,b,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,b,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,b,b,b,b,b,b,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a},
                    {a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a}

            };

            Image[][] grid2 = {
                    {o,o,o,aa,o,o,o,o,o,e,f,o,o,o,o,o,o,o,o,o,p,o,o,o,o,o,o,o,o,o},
                    {o,ah,o,o,ah,o,o,ah,o,n,o,o,o,n,o,ai,o,o,o,o,q,o,o,o,ab,o,n,p,o,o},
                    {o,o,o,o,o,o,o,m,o,o,o,o,o,o,o,o,o,o,o,o,o,n,o,o,o,o,o,q,o,n},
                    {af,o,o,o,r,s,o,o,o,n,af,o,n,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {ag,o,ah,o,o,o,ah,o,o,o,ag,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,p,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,aa,o,o,o,ai,o,o,o,o,o,o,o,o,o,o,q,o,o},
                    {o,ah,o,m,o,v,o,af,o,o,o,c,d,o,o,aa,o,n,o,o,o,ab,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,n,o,ag,o,o,o,e,f,o,o,o,o,o,aj,ak,o,o,o,o,o,o,o,o,n,p},
                    {o,o,n,o,o,o,o,o,ai,o,o,o,o,ai,o,o,o,o,al,am,ai,o,o,o,o,o,o,ai,o,q},
                    {o,ab,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,n,o,o,o,ai,o,o,o,o,o,o},
                    {d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,c,d,o,o,o,o,p,o,o},
                    {f,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,ab,o,o,o,e,f,o,o,o,o,q,ai,o},
                    {o,o,o,o,o,o,n,o,ai,o,o,p,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,p,o,o,ab,o,q,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,n,o,o},
                    {o,o,o,o,o,o,q,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,c},
                    {ai,o,o,o,o,o,o,o,o,o,n,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,p,n,e},
                    {o,o,o,o,o,o,o,o,o,o,o,c,d,o,o,o,o,o,o,o,o,o,p,o,o,o,o,q,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,e,f,o,n,o,p,o,o,o,o,o,q,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,p,o,n,o,o,p,o,aa,o,ab,o},
                    {aa,o,o,o,ad,o,o,o,o,o,o,ai,o,p,o,o,o,o,o,q,o,o,o,o,q,o,n,o,o,o},
                    {o,o,ac,o,ae,o,ac,o,o,o,o,o,o,q,o,o,ab,o,o,o,o,o,o,ai,o,o,o,o,c,d},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,p,o,o,o,c,d,o,o,o,o,ai,o,e,f},
                    {o,o,ac,o,ac,o,ac,o,o,o,o,o,n,o,o,o,q,o,o,o,e,f,o,n,o,o,o,o,o,o},
                    {u,o,o,o,o,o,o,o,o,o,o,ai,o,o,ab,o,o,ai,o,o,o,o,o,o,o,o,o,o,o,o},
                    {c,d,c,d,c,d,o,aa,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {x,y,x,y,x,y,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {y,x,y,x,y,x,y,d,c,d,c,d,c,d,c,d,c,d,c,d,c,d,c,d,c,d,c,d,c,d},
                    {x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y},
                    {y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x},
                    {x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y,x,y}
            };

            GridPane gridPane = new GridPane();

            // for visualizing the different squares:

            gridPane.setStyle("-fx-background-color: grey;");

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    ImageView imageView = new ImageView(grid[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            for (int y = 0 ; y < grid2.length ; y++) {
                for (int x = 0 ; x < grid2[y].length ; x++) {
                    if(grid2[y][x] == o) continue;
                    ImageView imageView = new ImageView(grid2[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            createMap(difficulty);
            ArrayList<Case> array = map.getMap();

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    if(grid[y][x] != b) continue;
                    for(Case c : array) {
                        if (c.getCoordY() == y && c.getCoordX() == x && c.isBonus()) {
                            ImageView imageView = new ImageView(z1);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                        else if(c.getCoordY() == y && c.getCoordX() == x && c.isMalus()){
                            ImageView imageView = new ImageView(z2);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                    }
                }
            }

            AnchorPane FirstAnchor = new AnchorPane();
            FirstAnchor.getChildren().addAll(gridPane);
            return FirstAnchor;
        }

        else{

            Image[][] grid = {
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a},
                    {a,a,a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a},
                    {b,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,b,b,b,b,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,b,a,a,a},
                    {a,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,b,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a},
                    {a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a,a}

            };

            Image[][] grid2 = {
                    {c,d,o,o,o,q,o,o,o,c,d,o,n,o,o,o,o,q,c,d,o,o,q,o,o,o,o,o,o,o},
                    {e,f,o,o,r,s,o,ai,o,e,f,o,o,o,o,ai,o,o,e,f,ai,o,o,o,o,o,o,ai,o,o},
                    {o,ai,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,n,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,ah,o,p,o,o,o,o,o,o,o,p,o,o,o,o,o,o,o,o},
                    {o,p,o,o,o,o,p,o,ai,o,o,o,o,q,o,ai,n,o,ah,o,o,q,af,o,af,o,ai,o,n,o},
                    {o,q,o,o,o,o,q,o,o,o,o,o,o,o,o,ah,o,o,o,o,o,o,ag,o,ag,o,o,c,d,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,ah,o,o,o,p,o,o,ah,o,o,o,o,o,p,o,e,f,o},
                    {c,d,o,o,o,o,o,o,o,o,o,o,o,o,o,o,q,o,o,o,o,ai,o,o,o,q,o,o,o,o},
                    {e,f,o,n,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,ai,o},
                    {o,o,o,o,o,o,r,s,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,ai,o,o,o,o},
                    {o,p,o,ai,o,c,d,o,o,ai,o,o,p,o,o,ai,o,c,d,o,o,ai,o,o,o,o,o,c,d,n},
                    {o,q,o,o,o,e,f,o,n,o,o,o,q,o,o,o,o,e,f,o,o,o,o,o,o,m,o,e,f,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,ai,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,c,d,o,m,o,o,o,o,o,o,o,o,o,o,p},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,e,f,o,o,o,c,d,o,o,o,o,o,o,o,q},
                    {d,o,o,ai,o,o,n,o,o,o,n,o,o,o,o,o,o,o,n,o,e,f,o,o,p,o,o,o,ai,o},
                    {f,o,o,o,o,p,o,o,ai,o,o,o,o,o,o,o,ai,o,o,ai,o,o,o,o,q,o,o,o,m,o},
                    {o,o,m,o,o,q,o,o,o,o,o,o,o,o,o,o,i,j,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,k,l,o,o,i,j,o,o,ai,o,o,o,c,d},
                    {o,o,ai,o,o,o,o,o,o,o,o,o,o,o,o,g,o,o,o,o,k,l,o,o,o,o,o,o,e,f},
                    {o,o,o,h,h,h,h,h,h,h,h,h,h,h,h,h,o,o,o,aj,ak,o,ai,o,o,o,o,o,o,o},
                    {o,o,o,h,h,h,h,h,h,h,h,h,h,h,h,h,o,ai,o,al,am,i,j,o,p,o,o,ai,o,o},
                    {o,o,g,h,h,h,h,h,h,h,h,h,h,h,h,h,o,o,o,o,o,k,l,o,q,o,o,o,o,c},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,m,e},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o},
                    {o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,o,ai,o},
                    {o,c,d,o,ai,o,o,ai,o,c,d,r,s,o,o,p,ai,o,o,o,o,p,o,o,o,c,d,o,o,o},
                    {o,e,f,o,o,p,o,n,o,e,f,o,o,o,o,q,o,c,d,o,o,q,o,ai,o,e,f,o,n,o},
                    {o,o,r,s,o,q,o,o,o,o,o,o,ai,o,o,o,o,e,f,o,o,o,o,o,o,o,o,r,s,o}
            };

            GridPane gridPane = new GridPane();

            // for visualizing the different squares:

            gridPane.setStyle("-fx-background-color: grey;");

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    ImageView imageView = new ImageView(grid[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            for (int y = 0 ; y < grid2.length ; y++) {
                for (int x = 0 ; x < grid2[y].length ; x++) {
                    if(grid2[y][x] == o) continue;
                    ImageView imageView = new ImageView(grid2[y][x]);
                    imageView.setFitWidth(tileSize);
                    imageView.setFitHeight(tileSize);
                    gridPane.add(imageView, x, y);
                }
            }

            createMap(difficulty);
            ArrayList<Case> array = map.getMap();

            for (int y = 0 ; y < grid.length ; y++) {
                for (int x = 0 ; x < grid[y].length ; x++) {
                    if(grid[y][x] != b) continue;
                    for(Case c : array) {
                        if (c.getCoordY() == y && c.getCoordX() == x && c.isBonus()) {
                            ImageView imageView = new ImageView(z1);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                        else if(c.getCoordY() == y && c.getCoordX() == x && c.isMalus()){
                            ImageView imageView = new ImageView(z2);
                            imageView.setFitWidth(tileSize);
                            imageView.setFitHeight(tileSize);
                            gridPane.add(imageView, x, y);
                        }
                    }
                }
            }

            AnchorPane FirstAnchor = new AnchorPane();
            FirstAnchor.getChildren().addAll(gridPane);
            return FirstAnchor;
        }
    }

    /**
     * Method to get the map
     * @return map
     */
    public Map getMap(){
        return this.map;
    }

    /**
     * Method to get the start case
     * @return start case
     */
    public Case getCaseStart(){
        return map.getStartCase();
    }

    /**
     * Method which give the next case
     * @param c
     * @return next case
     */
    public Case getNextCase(Case c){
        return map.getNextCase(c);
    }

    /**
     * Method which give the start case
     * @return start case
     */
    public Case getStartCase(){
        return map.getStartCase();
    }

    /**
     * Method to get the case which is num case after the case c
     * @param c
     * @param num
     * @return
     */
    public Case getCase(Case c, int num){
        return map.getCase(c, num);
    }
}
