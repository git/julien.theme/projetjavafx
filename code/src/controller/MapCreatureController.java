package controller;

import model.Case;
import model.Map;
import model.MapCreature;

public class MapCreatureController {
    private int lv;
    private MapCreature mapCreature;
    private Map map;
    private int[][] coordList;

    /**
     * Constructor of MapCreatureController class
     * @param lv
     * @param map
     */
    public MapCreatureController(int lv, Map map) {
        this.lv = lv;
        this.map = map;
    }

    /**
     * Method to create a MapCreature
     */
    public void creerMapCreature() {
        int[][] coord1Lv1 = {{0,-1},{0,0},{1,0},{2,0},{3,0},{3,1},{4,1},{5,1},{6,1},{7,1},{8,1},{9,1},{9,2},{10,2},{11,2},{12,2},{13,2},{14,2},{15,2},{16,2},{17,2},{18,2},{19,2},{20,2},{21,2},{21,3},{21,4},{22,4},{23,4},{24,4},{25,4},{25,5},{25,6},{26,6},{27,6},{28,6},{28,7},{28,8},{28,9},{28,10},{28,11},{28,12},{29,12},{30,12}};
        int[][] coord2Lv1 = {{30,27},{29,27},{28,27},{27,27},{26,27},{25,27},{24,27},{23,27},{23,26},{22,26},{21,26},{20,26},{20,25},{19,25},{18,25},{17,25},{16,25},{15,25},{14,25},{13,25},{13,24},{12,24},{11,24},{10,24},{9,24},{8,24},{7,24},{6,24},{6,25},{6,26},{5,26},{4,26},{3,26},{2,26},{1,26},{-1,26},{-2,26}};
        int[][] coord1Lv2 = {{0,-1},{0,0},{0,1},{0,2},{1,2},{2,2},{3,2},{3,3},{3,4},{3,5},{4,5},{5,5},{6,5},{7,5},{8,5},{8,6},{9,6},{9,7},{9,8},{10,8},{11,8},{12,8},{12,9},{13,9},{14,9},{15,9},{16,9},{17,9},{18,9},{18,10},{19,10},{20,10},{20,9},{21,9},{21,8},{21,7},{20,7},{20,6},{19,6},{18,6},{18,7},{17,7},{16,7},{15,7},{14,7},{14,6},{14,5},{14,4},{15,4},{15,3},{15,2},{14,2},{14,1},{14,0},{14,-1},{14,-2}};
        int[][] coord2Lv2 = {{30,22},{29,22},{28,22},{27,22},{26,22},{25,22},{24,22},{24,21},{23,21},{22,21},{22,20},{21,20},{20,20},{19,20},{18,20},{17,20},{17,19},{16,19},{15,19},{14,19},{14,18},{13,18},{13,17},{13,16},{14,16},{15,16},{16,16},{17,16},{17,17},{18,17},{19,17},{20,17},{21,17},{21,16},{21,15},{22,15},{23,15},{24,15},{25,15},{26,15},{26,14},{27,14},{28,14},{28,13},{28,12},{29,12},{30,12}};
        int[][] coord1Lv3 = {{30,4},{29,4},{28,4},{27,4},{26,4},{25,4},{24,4},{23,4},{23,5},{23,6},{23,7},{22,7},{21,7},{20,7},{20,6},{19,6},{19,5},{19,4},{18,4},{17,4},{17,5},{17,6},{17,7},{18,7},{18,8},{19,8},{20,8},{21,8},{22,8},{23,8},{24,8},{24,9},{25,9},{26,9},{27,9},{28,9},{29,9},{30,9}};
        int[][] coord2Lv3 = {{-1,7},{0,7},{1,7},{2,7},{2,8},{3,8},{4,8},{4,9},{5,9},{6,9},{7,9},{8,9},{9,9},{9,10},{10,10},{11,10},{12,10},{13,10},{14,10},{14,11},{14,12},{15,12},{15,13},{14,13},{13,13},{12,13},{11,13},{11,12},{11,11},{11,10},{10,10},{9,10},{8,10},{8,11},{7,11},{7,12},{7,13},{6,13},{5,13},{4,13},{4,12},{4,11},{4,10},{3,10},{2,10},{1,10},{0,10},{-1,10}};

        mapCreature = new MapCreature(map.getSizeTile(), map.getSizeMap());

        int max = 2;
        int min = 1;
        double a = Math.random() * ( max - min );
        int numCreature = (int) Math.round(a) + min;
        int j = 1;
        if(lv == 1) {
            if(numCreature == 1) {
                coordList = coord1Lv1;
            }
            if (numCreature == 2) {
                coordList = coord2Lv1;
            }
            for(int[] i : coordList){
                boolean bool = j == 1;
                Case case1 = new Case(j, i[0], i [1], bool,false,false, false, map);
                mapCreature.addCase(case1);
                j++;
            }
        }
        if(lv == 2) {
            if(numCreature == 1) {
                coordList = coord1Lv2;
            }
            if (numCreature == 2) {
                coordList = coord2Lv2;
            }
            for(int[] i : coordList){
                boolean bool = j == 1;
                Case case1 = new Case(j, i[0], i [1], bool,false,false, false, map);
                mapCreature.addCase(case1);
                j++;
            }
        }
        if(lv == 3) {
            if(numCreature == 1) {
                coordList = coord1Lv3;
            }
            if (numCreature == 2) {
                coordList = coord2Lv3;
            }
            for(int[] i : coordList){
                boolean bool = j == 1;
                Case case1 = new Case(j, i[0], i [1], bool,false,false, false, map);
                mapCreature.addCase(case1);
                j++;
            }
        }
    }

    /**
     * Method to get the map
     * @return creature's map
     */
    public MapCreature getMap(){
        return this.mapCreature;
    }

    /**
     * Method to get the next case
     * @param c
     * @return case
     */
    public Case getNextCase(Case c){
        return mapCreature.getNextCase(c);
    }

    /**
     * Method to get the start case
     * @return case
     */
    public Case getStartCase(){
        return mapCreature.getStartCase();
    }

    /**
     * Method to get the size of ListeCase
     * @return size of ListeCase
     */
    public int getSizeListeCase(){
        return coordList.length;
    }
}
