package controller;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import model.Avatar;
import model.Case;
import model.Map;

public class PersoController {

    @FXML
    public ImageView persoImageView;

    private Avatar avatar;
    private Case actualCase;
    private Case oldCase;

    /**
     * Method to create the avatar
     * @param difficulty
     * @param perso
     * @param map
     */
    public void createAvatar(int difficulty, int perso, Map map) {
        switch (perso) {
            case 1:
                this.avatar = new Avatar(1, "image/personnage/Perso1.",map.getCoordStartX(), map.getCoordStartY());
                break;
            case 2:
                this.avatar = new Avatar(2, "image/personnage/Perso2.", map.getCoordStartX(), map.getCoordStartY());
                break;
            case 3:
                this.avatar = new Avatar(3, "image/personnage/Perso3.", map.getCoordStartX(), map.getCoordStartY());
                break;
            default:
                System.out.println("Erreur de choix du personnage [createPerso] unknown number");
        }
    }

    /**
     * Method to initialize the avatar
     * @param difficulty
     * @param perso
     * @param map
     * @return the image of the avatar
     */
    public AnchorPane playerInitialisation(int difficulty, int perso, Map map) {

        createAvatar(difficulty, perso, map);

        String imagePath = getPath() + "2.png";
        AnchorPane test = new AnchorPane();
        persoImageView = new ImageView(new Image(imagePath));
        persoImageView.setId("persoImageView");
        persoImageView.setFitWidth(35);
        persoImageView.setFitHeight(35);
        persoImageView.setLayoutX(getX()*map.getSizeTile()-5);
        persoImageView.setLayoutY(getY()*map.getSizeTile()-5);
        test.getChildren().add(persoImageView);
        return test;
    }

    /**
     * Method to get the path of the image
     * @return the path of the image
     */
    public String getPath(){
        return avatar.getImage();
    }

    /**
     * Method to get the X coordinate of the avatar
     * @return the X coordinate of the avatar
     */
    public int getX(){
        return avatar.getCoordX();

    }

    /**
     * Method to get the Y coordinate of the avatar
     * @return the Y coordinate of the avatar
     */
    public int getY(){
        return avatar.getCoordY();
    }

    /**
     * Method to set the actual case of the avatar
     * @param actual
     */
    public void setActualCase(Case actual){
        this.actualCase = actual;
    }

    /**
     * Method to get the actual case of the avatar
     * @return
     */
    public Case getActualCase() {
        return this.actualCase;
    }

    public Case getOldCase() {
        return this.oldCase;
    }

    /**
     * Method to set the old case of the avatar
     * @param old
     */
    public void setOldCase(Case old){
        this.oldCase = old;
    }
}
