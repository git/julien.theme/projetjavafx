package controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import model.ListeScore;
import model.ResultatTxt;
import model.Score;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class TopPlayerController implements Initializable {

    @FXML
    private AnchorPane levelRoot;

    @FXML
    private Button returnToMenu;
    private Text resultat1;
    private Text resultat2;
    private Text resultat3;
    private Text resultat4;
    private Text resultat5;
    private Text resultat6;
    private Text resultat7;
    private Text resultat8;
    private Text resultat9;
    private Text resultat10;

    /**
     * Initializes the controller class.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        initialisationTexte();
        afficherRésultat();
    }

    /**
     * Method to initialize the text
     */
    public void initialisationTexte(){
        resultat1 = new Text();
        resultat1.setText("");
        resultat1.setX(300);
        resultat1.setY(210);
        resultat1.setFill(Color.WHITE);
        resultat1.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat2 = new Text();
        resultat2.setText("");
        resultat2.setX(300);
        resultat2.setY(240);
        resultat2.setFill(Color.WHITE);
        resultat2.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat3 = new Text();
        resultat3.setText("");
        resultat3.setX(300);
        resultat3.setY(270);
        resultat3.setFill(Color.WHITE);
        resultat3.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat4 = new Text();
        resultat4.setText("");
        resultat4.setX(300);
        resultat4.setY(300);
        resultat4.setFill(Color.WHITE);
        resultat4.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat5 = new Text();
        resultat5.setText("");
        resultat5.setX(300);
        resultat5.setY(330);
        resultat5.setFill(Color.WHITE);
        resultat5.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat6 = new Text();
        resultat6.setText("");
        resultat6.setX(300);
        resultat6.setY(360);
        resultat6.setFill(Color.WHITE);
        resultat6.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat7 = new Text();
        resultat7.setText("");
        resultat7.setX(300);
        resultat7.setY(390);
        resultat7.setFill(Color.WHITE);
        resultat7.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat8 = new Text();
        resultat8.setText("");
        resultat8.setX(300);
        resultat8.setY(420);
        resultat8.setFill(Color.WHITE);
        resultat8.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat9 = new Text();
        resultat9.setText("");
        resultat9.setX(300);
        resultat9.setY(450);
        resultat9.setFill(Color.WHITE);
        resultat9.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");
        resultat10 = new Text();
        resultat10.setText("");
        resultat10.setX(300);
        resultat10.setY(480);
        resultat10.setFill(Color.WHITE);
        resultat10.setStyle("-fx-font: 20 Arial; -fx-alignment: CENTER;");


        levelRoot.getChildren().addAll(resultat1,resultat2,resultat3,resultat4,resultat5,resultat6,resultat7,resultat8,resultat9,resultat10);
    }

    /**
     * Method to display the result
     */
    public void afficherRésultat(){
        ResultatTxt resultatTxt = new ResultatTxt();
        ListeScore listeScore = new ListeScore();
        ArrayList<Score> liste = new ArrayList<>();
        listeScore = resultatTxt.chargerResultat();
        liste = listeScore.getListeScore();
        int meilleur1 = 0;
        int meilleur2 = 0;
        int meilleur3 = 0;
        int meilleur4 = 0;
        int meilleur5 = 0;
        int meilleur6 = 0;
        int meilleur7 = 0;
        int meilleur8 = 0;
        int meilleur9 = 0;
        int meilleur10 = 0;
        String valeur1 = "";
        String valeur2 = "";
        String valeur3 = "";
        String valeur4 = "";
        String valeur5 = "";
        String valeur6 = "";
        String valeur7 = "";
        String valeur8 = "";
        String valeur9 = "";
        String valeur10 = "";

        //make a hashmap
        HashMap<Integer, String> map = new HashMap<>();
        String value = "";
        int i = 0;

        for (Score score : liste) {
            value = "Lvl : " + score.getLevel() + " Date : " + score.getDate();
            map.put(score.getScore(), value);
            //System.out.println(score.getScore() + " " + score.getLevel() + " " + score.getDate());
        }
        for(Map.Entry<Integer, String> entry : map.entrySet()){
            Integer key = entry.getKey();
            String valeur = entry.getValue();

            System.out.println(i + " " + key + " " + valeur);
            switch (i){
                case 0:
                    meilleur1 = key;
                    valeur1 = valeur;
                    break;
                case 1:
                    meilleur2 = key;
                    valeur2 = valeur;
                    break;
                case 2:
                    meilleur3 = key;
                    valeur3 = valeur;
                    break;
                case 3:
                    meilleur4 = key;
                    valeur4 = valeur;
                    break;
                case 4:
                    meilleur5 = key;
                    valeur5 = valeur;
                    break;
                case 5:
                    meilleur6 = key;
                    valeur6 = valeur;
                    break;
                case 6:
                    meilleur7 = key;
                    valeur7 = valeur;
                    break;
                case 7:
                    meilleur8 = key;
                    valeur8 = valeur;
                    break;
                case 8:
                    meilleur9 = key;
                    valeur9 = valeur;
                    break;
                case 9:
                    meilleur10 = key;
                    valeur10 = valeur;
                    break;
            }
            if(key <= meilleur1){
                System.out.println(key + " : key >= meilleur1");
                meilleur10 = meilleur9;
                valeur10 = valeur9;
                meilleur9 = meilleur8;
                valeur9 = valeur8;
                meilleur8 = meilleur7;
                valeur8 = valeur7;
                meilleur7 = meilleur6;
                valeur7 = valeur6;
                meilleur6 = meilleur5;
                valeur6 = valeur5;
                meilleur5 = meilleur4;
                valeur5 = valeur4;
                meilleur4 = meilleur3;
                valeur4 = valeur3;
                meilleur3 = meilleur2;
                valeur3 = valeur2;
                meilleur2 = meilleur1;
                valeur2 = valeur1;
                meilleur1 = key;
                valeur1 = valeur;
            }else{
                if(key <= meilleur2){
                    System.out.println(key + " : key >= meilleur2");
                    meilleur10 = meilleur9;
                    valeur10 = valeur9;
                    meilleur9 = meilleur8;
                    valeur9 = valeur8;
                    meilleur8 = meilleur7;
                    valeur8 = valeur7;
                    meilleur7 = meilleur6;
                    valeur7 = valeur6;
                    meilleur6 = meilleur5;
                    valeur6 = valeur5;
                    meilleur5 = meilleur4;
                    valeur5 = valeur4;
                    meilleur4 = meilleur3;
                    valeur4 = valeur3;
                    meilleur3 = meilleur2;
                    valeur3 = valeur2;
                    meilleur2 = key;
                    valeur2 = valeur;
                }else{
                    if(key <= meilleur3){
                        System.out.println(key + " : key >= meilleur3");
                        meilleur10 = meilleur9;
                        valeur10 = valeur9;
                        meilleur9 = meilleur8;
                        valeur9 = valeur8;
                        meilleur8 = meilleur7;
                        valeur8 = valeur7;
                        meilleur7 = meilleur6;
                        valeur7 = valeur6;
                        meilleur6 = meilleur5;
                        valeur6 = valeur5;
                        meilleur5 = meilleur4;
                        valeur5 = valeur4;
                        meilleur4 = meilleur3;
                        valeur4 = valeur3;
                        meilleur3 = key;
                        valeur3 = valeur;
                    }else{
                        if(key <= meilleur4){
                            System.out.println(key + " : key >= meilleur4");
                            meilleur10 = meilleur9;
                            valeur10 = valeur9;
                            meilleur9 = meilleur8;
                            valeur9 = valeur8;
                            meilleur8 = meilleur7;
                            valeur8 = valeur7;
                            meilleur7 = meilleur6;
                            valeur7 = valeur6;
                            meilleur6 = meilleur5;
                            valeur6 = valeur5;
                            meilleur5 = meilleur4;
                            valeur5 = valeur4;
                            meilleur4 = key;
                            valeur4 = valeur;
                        }else{
                            if(key <= meilleur5){
                                System.out.println(key + " : key >= meilleur5");
                                meilleur10 = meilleur9;
                                valeur10 = valeur9;
                                meilleur9 = meilleur8;
                                valeur9 = valeur8;
                                meilleur8 = meilleur7;
                                valeur8 = valeur7;
                                meilleur7 = meilleur6;
                                valeur7 = valeur6;
                                meilleur6 = meilleur5;
                                valeur6 = valeur5;
                                meilleur5 = key;
                                valeur5 = valeur;
                            }else{
                                if(key <= meilleur6){
                                    System.out.println(key + " : key >= meilleur6");
                                    meilleur10 = meilleur9;
                                    valeur10 = valeur9;
                                    meilleur9 = meilleur8;
                                    valeur9 = valeur8;
                                    meilleur8 = meilleur7;
                                    valeur8 = valeur7;
                                    meilleur7 = meilleur6;
                                    valeur7 = valeur6;
                                    meilleur6 = key;
                                    valeur6 = valeur;
                                }else{
                                    if(key <= meilleur7){
                                        System.out.println(key + " : key >= meilleur7");
                                        meilleur10 = meilleur9;
                                        valeur10 = valeur9;
                                        meilleur9 = meilleur8;
                                        valeur9 = valeur8;
                                        meilleur8 = meilleur7;
                                        valeur8 = valeur7;
                                        meilleur7 = key;
                                        valeur7 = valeur;
                                    }else{
                                        if(key <= meilleur8){
                                            System.out.println(key + " : key >= meilleur8");
                                            meilleur10 = meilleur9;
                                            valeur10 = valeur9;
                                            meilleur9 = meilleur8;
                                            valeur9 = valeur8;
                                            meilleur8 = key;
                                            valeur8 = valeur;
                                        }else{
                                            if(key <= meilleur9){
                                                System.out.println(key + " : key >= meilleur9");
                                                meilleur10 = meilleur9;
                                                valeur10 = valeur9;
                                                meilleur9 = key;
                                                valeur9 = valeur;
                                            }else{
                                                if(key <= meilleur10){
                                                    System.out.println(key + " : key >= meilleur10");
                                                    meilleur10 = key;
                                                    valeur10 = valeur;

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            i++;
        }
        if (meilleur10 != 0 && i>=10) {
            resultat10.setText("Temps : " + meilleur10 + " " + valeur10);
        }
        if (meilleur9 != 0 && i>=9) {
            resultat9.setText("Temps : " + meilleur9 + " " + valeur9);
        }
        if (meilleur8 != 0 && i>=8) {
            resultat8.setText("Temps : " + meilleur8 + " " + valeur8);
        }
        if (meilleur7 != 0 && i>=7) {
            resultat7.setText("Temps : " + meilleur7 + " " + valeur7);
        }
        if (meilleur6 != 0 && i>=6) {
            resultat6.setText("Temps : " + meilleur6 + " " + valeur6);
        }
        if (meilleur5 != 0&& i>=5) {
            resultat5.setText("Temps : " + meilleur5 + " " + valeur5);
        }
        if (meilleur4 != 0 && i>=4) {
            resultat4.setText("Temps : " + meilleur4 + " " + valeur4);
        }
        if (meilleur3 != 0 && i>=3) {
            resultat3.setText("Temps : " + meilleur3 + " " + valeur3);
        }
        if (meilleur2 != 0 && i>=2) {
            resultat2.setText("Temps : " + meilleur2 + " " + valeur2);
        }
        if (meilleur1 != 0) {
            resultat1.setText("Temps : " + meilleur1 + " " + valeur1);
        }
    }

    /**
     * Méthod to return to the menu
     * @param event
     * @throws Exception
     */
    @FXML
    void returnToMenu(MouseEvent event) throws  Exception{
        AnchorPane pane = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/MainPage.fxml")));
        levelRoot.getChildren().setAll(pane);
    }

}
