package launcher;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.*;
import java.util.ArrayList;
import java.util.Objects;

public class Main extends Application {

    private Album song;

    @Override
    public void start(Stage primaryStage) throws Exception {

        Parent root = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("/fxml/MainPage.fxml")));
        primaryStage.setTitle("The Adventurer");
        primaryStage.setScene(new Scene(root, 900, 600));
        primaryStage.setResizable(false);
        primaryStage.show();

        //début musique
        ArrayList<Musique> musiqueListe = new ArrayList<>();
        Musique song1 = new Musique("/musique/medieval_musique1.mp3",211000);
        Musique song2 = new Musique("/musique/medieval_musique2.mp3",248000);
        Musique song3 = new Musique("/musique/medieval_musique3.mp3",248000);
        Musique song4 = new Musique("/musique/medieval_musique4.mp3",209000);
        song = new Album(musiqueListe);
        song.addList(song1);
        song.addList(song2);
        song.addList(song3);
        song.addList(song4);
        song.play();
    }

    /**
     * Method to stop the music
     */
    @Override
    public void stop(){
        song.stop();
    }

    public static void main(String[] args) {
        launch(args);
    }
}

