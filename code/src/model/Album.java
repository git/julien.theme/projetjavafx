package model;

import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Album {
    private ArrayList<Musique> listeMusiques;
    private Thread musicThread;

    /**
     * Constructor for the Album class
     * @param listeMusiques
     */
    public Album(ArrayList<Musique> listeMusiques) {
        this.listeMusiques = listeMusiques;
    }

    /**
     * Method that plays the music
     */
    public void play(){

        musicThread = new Thread("musique") {
            public void run(){
                int i;
                Media media;
                AudioClip mediaPlayer;
                while(true){
                    for (Musique musique : listeMusiques) {
                        media = new Media(Objects.requireNonNull(getClass().getResource(musique.getPath())).toExternalForm());
                        mediaPlayer = new AudioClip(media.getSource());
                        mediaPlayer.volumeProperty().setValue(0.025);
                        mediaPlayer.play();
                        try {
                            Thread.sleep(musique.getTemps());
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        musicThread.setDaemon(true);
        musicThread.start();


    }

    /**
     * Method that stops the music
     */
    public void stop() {
        musicThread.stop();
    }

    /**
     * Method that add a music to the list
     * @param musique
     */
    public void addList(Musique musique){
        listeMusiques.add(musique);
    }

    /**
     * Method that remove a music from the list
     * @param musique
     */
    public void removeList(Musique musique){
        listeMusiques.remove(musique);
    }

    /**
     * Method that return the list of music
     * @return  listeMusiques
     */
    public List<Musique> getListeMusiques() {
        return listeMusiques;
    }
}
