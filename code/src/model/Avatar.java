package model;

public class Avatar {
    private int idAvatar;
    private String image;
    private int coordX;
    private int coordY;

    /**
     * Constructor for the Avatar class
     * @param idAvatar
     * @param image
     * @param coordX
     * @param coordY
     */
    public Avatar(int idAvatar, String image, int coordX, int coordY) {
        this.idAvatar = idAvatar;
        this.image = image;
        this.coordX = coordX;
        this.coordY = coordY;
    }

    /**
     * Method to get the ID of the avatar
     * @return idAvatar
     */
    public int getIdAvatar() {
        return idAvatar;
    }

    /**
     * Method to get the image of the avatar
     * @return image
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Method to get the X coordinate of the avatar
     * @return coordX
     */
    public int getCoordX() {
        return coordX;
    }

    /**
     * Method to get the Y coordinate of the avatar
     * @return coordY
     */
    public int getCoordY() {
        return coordY;
    }

    /**
     * Method to set the ID of the avatar
     * @param idAvatar
     */
    public void setIdAvatar(int idAvatar) {
        this.idAvatar = idAvatar;
    }

    /**
     * Method to set the image of the avatar
     * @param image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Method to set the X coordinate of the avatar
     * @param coordX
     */
    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    /**
     * Method to set the Y coordinate of the avatar
     * @param coordY
     */
    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    /**
     * Method to get the coordinates of the avatar
     * @param coordX
     * @param coordY
     */
    public void setCoord(int coordX, int coordY){
        this.coordX = coordX;
        this.coordY = coordY;
    }
}
