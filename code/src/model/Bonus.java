package model;

public enum Bonus {

    MOVE_FORWARD_1,
    MOVE_FORWARD_3,
    MOVE_FORWARD_5,
    MOVE_FORWARD_8,
    NEW_DICE;

    
    /**
     * Method that returns the bonus associated to the given string.
     * @return the bonus associated to the given string.
     */
    public static Bonus randomGet(){
        int i = (int) (Math.random() * 8);
        switch (i) {
            case 0:
            case 1:
            case 2:
                return Bonus.MOVE_FORWARD_1;

            case 3:
            case 4:
                return Bonus.MOVE_FORWARD_3;

            case 5:
            case 6:
                return Bonus.MOVE_FORWARD_5;

            case 7:
                return Bonus.MOVE_FORWARD_8;

            case 8:
                return Bonus.NEW_DICE;
        }
        return null;
    }
}
