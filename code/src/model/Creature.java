package model;

public class Creature {
    int idCreature;
    String image;
    int coordX;
    int coordY;

    /**
     * Constructor of Creature class
     * @param idCreature
     * @param image
     * @param coordX
     * @param coordY
     */
    public Creature(int idCreature, String image, int coordX, int coordY) {
        this.idCreature = idCreature;
        this.image = image;
        this.coordX = coordX;
        this.coordY = coordY;
    }

    /**
     * Method to get the id of the creature
     * @return  idCreature
     */
    public int getIdCreature() {
        return idCreature;
    }

    /**
     * Method to get the image of the creature
     * @return image
     */
    public String getImage() {
        return image;
    }

    /**
     * Method to get the x coordinate of the creature
     * @return coordX
     */
    public int getCoordX() {
        return coordX;
    }

    /**
     * Method to get the y coordinate of the creature
     * @return coordY
     */
    public int getCoordY() {
        return coordY;
    }

    /**
     * Method to set the x coordinate of the creature
     * @param coordX
     */
    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    /**
     * Method to set the y coordinate of the creature
     * @param coordY
     */
    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }
}
