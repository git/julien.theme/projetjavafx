package model;

import controller.CreatureController;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

import javafx.scene.image.ImageView;

public class GameLoop {
    private Thread timer;
    private int lv;
    private Map map;
    private CreatureController c;
    @FXML public ImageView creatureImageView;

    /**
     * Constructor for GameLoop class
     * @param lv
     * @param map
     * @param creatureImageView
     */
    public GameLoop(int lv, Map map, ImageView creatureImageView){
        this.lv = lv;
        this.map = map;
        this.creatureImageView = creatureImageView;
    }

    /**
     * Method to start the game loop
     */
    public void start() {
        c = new CreatureController(lv,map, creatureImageView);
        timer = new Thread("timer") {
            public void run() {
                int nbSeconde = 50;
                int i = 0;
                while(true) {
                    if(i == 0) {
                        try {
                            c.playerInitialisation();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        //System.out.println("Timer = " + i);
                        i =  nbSeconde;
                        try {
                            timer.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }else{
                        //System.out.println("Timer = " + i);
                        i--;
                        try {
                            timer.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }

                }
            }


        };
        timer.start();
    }

    /**
     * Method to stop the game loop
     */
    public void stop() {
        System.out.println("GameLoop.stop()");
        timer.stop();
        timer.interrupt();
    }
}
