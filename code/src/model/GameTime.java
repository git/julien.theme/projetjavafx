package model;

import controller.CreatureController;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;

public class GameTime {
    private Thread timer;
    private Text textTimer;

    /**
     * Constructor for GameTime class
     * @param textTimer
     */
    public GameTime(Text textTimer) {
        this.textTimer = textTimer;
    }

    public void start() {
        System.out.println("GameTime.start()");
        timer = new Thread("timer") {
            public void run() {
                int time = 0;
                while (true) {
                    textTimer.setText(String.valueOf(time));
                    time++;
                    try {
                        timer.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        timer.start();
    }

    /**
     * Method to stop the timer
     */
    public void stop() {
        System.out.println("GameTime.stop()");
        timer.stop();
    }

    /**
     * Method to get the time
     * @return time
     */
    public int getTime() {
        return Integer.parseInt(textTimer.getText());
    }
}


