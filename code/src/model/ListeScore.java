package model;

import java.awt.*;
import java.util.ArrayList;

public class ListeScore {
    private ArrayList listeScore = new ArrayList<Score>();

    /**
     * Method to add a score to the list of scores
     * @param score
     */
    public void addScore(Score score) {
        listeScore.add(score);
    }

    /**
     * Method to get the list of scores
     * @return
     */
    public ArrayList getListeScore() {
        return listeScore;
    }
}
