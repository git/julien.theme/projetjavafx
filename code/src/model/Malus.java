package model;

public enum Malus {

    MOVE_BACKWARD_1, // Recule de 1
    MOVE_BACKWARD_3, // Recule de 3
    MOVE_BACKWARD_5, // Recule de 5
    LAST_POSITION; // Retour a la position initiale

    /**
     * Method to get the malus
     * @return the malus
     */
    public static Malus randomGet(){
        int i = (int) (Math.random() * 8);
        switch (i) {
            case 0:
            case 1:
            case 2:
                return Malus.MOVE_BACKWARD_1;

            case 3:
            case 4:
                return Malus.MOVE_BACKWARD_3;

            case 5:
            case 6:
                return Malus.MOVE_BACKWARD_5;

            case 7:
                return Malus.LAST_POSITION;
        }
        return null;
    }
}
