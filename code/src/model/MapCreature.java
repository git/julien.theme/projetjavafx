package model;

import java.util.ArrayList;

public class MapCreature {
    private final int sizeTile;
    private final int sizeMap;
    private final ArrayList<Case> map = new ArrayList<>();
    private int coordStartX;
    private int coordStartY;

    /**
     * Constructor of MapCreature class
     * @param sizeTile
     * @param sizeMap
     */
    public MapCreature(int sizeTile, int sizeMap) {
        this.coordStartX = getCoordStartX();
        this.coordStartY = getCoordStartY();
        this.sizeTile = sizeTile;
        this.sizeMap = sizeMap;
    }

    /**
     * Method to add a case to the map
     * @param c
     */
    public void addCase(Case c) {
        this.map.add(c);
        if(c.isStart){
            setCoordStartX(c.coordX);
            setCoordStartY(c.coordY);
        }
    }

    /**
     * Method to remove a case from the map
     * @param c
     */
    public void removeCase(Case c) {
        this.map.remove(c);
    }

    /**
     * Method to get the map
     * @return map
     */
    public ArrayList<Case> getMap() {
        return this.map;
    }

    /**
     * Method to get the size of the tile
     * @return sizeTile
     */
    public int getSizeTile() {
        return this.sizeTile;
    }

    /**
     * Method to get the size of the map
     * @return sizeMap
     */
    public int getSizeMap() {
        return this.sizeMap;
    }

    /**
     * Method to get the X coordinate of the start case
     * @return coordStartX
     */
    public int getCoordStartX(){
        return coordStartX;
    }

    /**
     * Method to set the X coordinate of the start case
     * @param x
     */
    public void setCoordStartX(int x){
        this.coordStartX = x;
    }

    /**
     * Method to get the Y coordinate of the start case
     * @return coordStartY
     */
    public int getCoordStartY(){
        return coordStartY;
    }

    /**
     * Method to set the Y coordinate of the start case
     * @param y
     */
    public void setCoordStartY(int y){
        this.coordStartY = y;
    }

    /**
     * Method to get the next case of the map
     * @param c
     * @return nextCase
     */
    public Case getNextCase(Case c){
        int i;
        int j;
        for(i = 0; i < this.map.size(); i++){
            if(c == this.map.get(i)){
                j=i+1;
                return this.map.get(j);
            }
        }
        return c;
    }

    /**
     * Method to get the start case of the map
     * @return startCase
     */
    public Case getStartCase(){
        for(Case c : this.map){
            if(c.isStart){
                System.out.println(c);
                return c;
            }
        }
        return null;
    }

}




