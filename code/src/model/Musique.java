package model;


import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

import java.util.Objects;

public class Musique {
    private String path;
    private int temps;

    /**
     * Constructor of the Musique class
     * @param path
     * @param temps
     */
    public Musique(String path , int temps) {
        this.path = path;
        this.temps = temps;
    }

    /**
     * Method to get the path of the music
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * Method to get the time of the music
     * @return temps
     */
    public int getTemps() {
        return temps;
    }
}
