package model;

import model.ListeScore;
import model.Score;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ResultatTxt {
    /**
     * Method to save the score in the txt file
     * @param resultat
     * @param lvl
     */
    public void sauvegarderResultat(int resultat, int lvl) {
        try {
            LocalDate today = LocalDate.now();
            FileWriter fw = new FileWriter("ressources/resultat/resultats.txt",true);
            fw.write(resultat + " " + lvl + " " + today.format(DateTimeFormatter.ofPattern("dd-MM-yy")) + "\n");
            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to load the score in the txt file
     * @return ListeScore
     */
    public ListeScore chargerResultat(){
        ListeScore ls = new ListeScore();
        try
        {
            File file = new File("ressources/resultat/resultats.txt");
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String line;
            String separateur = " ";
            String[] tab;
            int level;
            int resultat;
            String date;
            while((line = br.readLine()) != null)
            {
                tab = line.split(separateur);
                level = Integer.parseInt(tab[1]);
                resultat = Integer.parseInt(tab[0]);
                date = tab[2];
                Score sc = new Score(resultat, level, date);
                ls.addScore(sc);
            }
            fr.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return ls;
    }
}