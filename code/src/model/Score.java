package model;

import java.time.LocalDateTime;

public class Score {
    private int score;
    private int level;
    private String date;

    /**
     * Constructor for Score class
     * @param score
     * @param level
     * @param date
     */
    public Score(int score, int level,String date) {
        this.score = score;
        this.level = level;
        this.date = date;
    }

    /**
     * Method to get the level
     * @return level
     */
    public int getLevel() {
        return level;
    }

    /**
     * Method to get the score
     * @return score
     */
    public int getScore() {
        return score;
    }

    /**
     * Method to get the date
     * @return date
     */
    public String getDate() {
        return date;
    }
}
